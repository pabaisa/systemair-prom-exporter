# systemair-prom-exporter

A SystemAir HVAC unit data Prometheus exporter written in Python. Uses [sambus2019](https://github.com/Stogas/sambus2019)

# Installation
```
pip3 install -r requirements.txt
```

# Running in CLI
```
/usr/bin/python3 -u /opt/systemair-prom-exporter/systemair-prom-exporter.py -l <YOUR_LOCATION_LABEL> -p <EXPORTER_PORT>
```
