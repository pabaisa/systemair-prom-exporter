#!/usr/bin/env python3

import sambus2019.sambus as sambus
from prometheus_client import start_http_server, Gauge, Enum
import random
import time

SAMBUS_BAUDRATE = 19200
SAMBUS_TIMEOUT = 0.5
PORTNAME = '/dev/ttyUSB0'
ADDRESS = 1
LOCATION_LABEL = 'default'
PORT = 9999

def main(argv):
  import getopt

  try:
    opts, args = getopt.getopt(argv,"l:p:",["location-label=","port="])
  except getopt.GetoptError:
    print ('systemair-prom-exporter.py -l <location-label> -p <exporter-port>')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print ('systemair-prom-exporter.py -l <location-label> -p <exporter-port>')
      sys.exit()
    elif opt in ("-l", "--location-label"):
      LOCATION_LABEL = str(arg)
    elif opt in ("-p", "--exporter-port"):
      PORT = int(arg)

  sam = sambus.SAMbus(PORTNAME, ADDRESS)
  sam.serial.timeout = SAMBUS_TIMEOUT
  sam.serial.baudrate = SAMBUS_BAUDRATE

  temp_mode = Gauge('hvac_temp_mode_enabled', 'Temperature target mode', ['location','mode'])
  temp_mode.labels(location=LOCATION_LABEL, mode='supply').set_function(lambda: sam.get_temp_mode() == "Supply")
  temp_mode.labels(location=LOCATION_LABEL, mode='room').set_function(lambda: sam.get_temp_mode() == "Room")
  temp_mode.labels(location=LOCATION_LABEL, mode='extract').set_function(lambda: sam.get_temp_mode() == "Extract")

  temp_controller = Gauge('hvac_temp_controller_percentage', 'Temperature controller output %', ['location'])
  temp_controller.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_temp_demand())

  iaq = Gauge('hvac_iaq_level', 'IAQ current level', ['location','level'])
  iaq.labels(location=LOCATION_LABEL, level='Economic').set_function(lambda: sam.get_iaq_level() == "Economic")
  iaq.labels(location=LOCATION_LABEL, level='Good').set_function(lambda: sam.get_iaq_level() == "Good")
  iaq.labels(location=LOCATION_LABEL, level='Improving').set_function(lambda: sam.get_iaq_level() == "Improving")

  usermode = Gauge('hvac_usermode_enabled', 'Current User mode', ['location','mode'])
  usermode.labels(location=LOCATION_LABEL, mode='auto').set_function(lambda: sam.get_usermode() == "Auto")
  usermode.labels(location=LOCATION_LABEL, mode='manual').set_function(lambda: sam.get_usermode() == "Manual")
  usermode.labels(location=LOCATION_LABEL, mode='crowded').set_function(lambda: sam.get_usermode() == "Crowded")
  usermode.labels(location=LOCATION_LABEL, mode='refresh').set_function(lambda: sam.get_usermode() == "Refresh")
  usermode.labels(location=LOCATION_LABEL, mode='fireplace').set_function(lambda: sam.get_usermode() == "Fireplace")
  usermode.labels(location=LOCATION_LABEL, mode='away').set_function(lambda: sam.get_usermode() == "Away")
  usermode.labels(location=LOCATION_LABEL, mode='holiday').set_function(lambda: sam.get_usermode() == "Holiday")

  eco = Gauge('hvac_eco', 'ECO mode', ['location','state'])
  eco.labels(location=LOCATION_LABEL, state='enabled').set_function(lambda: sam.get_eco_enabled() == "Enabled")
  eco.labels(location=LOCATION_LABEL, state='active').set_function(lambda: sam.get_eco_active() == "Active")

  airfilter = Gauge('hvac_airfilter_remaining_seconds', 'Air filter time remaining until replaced', ['location'])
  airfilter.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_filter_remaining())

  freecooling = Gauge('hvac_freecooling', 'Free Cooling mode', ['location','state'])
  freecooling.labels(location=LOCATION_LABEL, state='enabled').set_function(lambda: sam.get_freecooling_enabled() == "Enabled")
  freecooling.labels(location=LOCATION_LABEL, state='active').set_function(lambda: sam.get_freecooling_active() == "Active")

  triac_active = Gauge('hvac_triac_active', 'TRIAC (heating) active boolean', ['location'])
  triac_active.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_triac_active() == "Active")
  triac_voltage = Gauge('hvac_triac_voltage', 'TRIAC (heating) voltage 0-10 V', ['location'])
  triac_voltage.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_triac_voltage())

  heater_active = Gauge('hvac_heater_active', 'Electric heater active boolean', ['location'])
  heater_active.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_heater_active() == "Active")
  heater_voltage = Gauge('hvac_heater_voltage', 'Electric heater voltage 0-10 V', ['location'])
  heater_voltage.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_heater_voltage())

  heatexchanger_active = Gauge('hvac_heatexchanger_active', 'Heat exchanger active boolean', ['location'])
  heatexchanger_active.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_heatexchanger_active() == "Active")
  heatexchanger_voltage = Gauge('hvac_heatexchangeer_voltage', 'Heat exchanger voltage 0-10 V', ['location'])
  heatexchanger_voltage.labels(location=LOCATION_LABEL).set_function(lambda: sam.get_heatexchanger_voltage())

  temp = Gauge('hvac_temp_degrees', 'Current values of temperature sensors', ['location','sensor'])
  temp.labels(location=LOCATION_LABEL, sensor='OAT').set_function(lambda: sam.get_temp_sensor_oat())
  temp.labels(location=LOCATION_LABEL, sensor='OHT').set_function(lambda: sam.get_temp_sensor_oht())
  temp.labels(location=LOCATION_LABEL, sensor='SAT').set_function(lambda: sam.get_temp_sensor_sat())
  temp.labels(location=LOCATION_LABEL, sensor='EAT').set_function(lambda: sam.get_temp_sensor_eat())

  temp_target = Gauge('hvac_temp_target_degrees', 'Target temperatures', ['location','type'])
  temp_target.labels(location=LOCATION_LABEL, type='room').set_function(lambda: sam.get_temp_target())
  temp_target.labels(location=LOCATION_LABEL, type='supply').set_function(lambda: sam.get_temp_target_supply())

  humid = Gauge('hvac_humidity_percentage', 'Humidity values', ['location','type'])
  humid.labels(location=LOCATION_LABEL, type='sensor').set_function(lambda: sam.get_rh_sensor())
  humid.labels(location=LOCATION_LABEL, type='demand ').set_function(lambda: sam.get_rh_demand())

  fan_rpm = Gauge('hvac_fan_speed_rpm', 'Current fan speed in RPM', ['location','fan'])
  fan_rpm.labels(location=LOCATION_LABEL, fan='SAF').set_function(lambda: sam.get_fan_saf_current_rpm())
  fan_rpm.labels(location=LOCATION_LABEL, fan='EAF').set_function(lambda: sam.get_fan_eaf_current_rpm())

  fan_perc = Gauge('hvac_fan_speed_percentage', 'Current fan speed in percentage', ['location','fan'])
  fan_perc.labels(location=LOCATION_LABEL, fan='SAF').set_function(lambda: sam.get_fan_saf_percentage())
  fan_perc.labels(location=LOCATION_LABEL, fan='EAF').set_function(lambda: sam.get_fan_eaf_percentage())

  start_http_server(PORT)
  print ( "Started Prometheus exporter on port " + str(PORT) + "..." )

  while True:
    pass

if __name__ == '__main__':
  import sys
  main(sys.argv[1:])
